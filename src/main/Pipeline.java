package main;
//TFG 2013-2014

import architecture.Architecture;
import architecture.Instruction;
import architecture.MIPS;
import architecture.Operations;
import java.io.IOException;
import java.util.ArrayList;
import files.ConfigFile;
import java.awt.Toolkit;
//import java.io.BufferedReader;
import java.io.FileNotFoundException;
//import java.io.FileReader;
import java.util.HashMap;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JScrollPane;

/**
 * Clase Pipeline. Aqui se ejecuta las instrucciones con la estructura segmentada 
 * explicada a continuación. Tendremos un Pipeline con 5 fases configurada debidamente.
 * En esta estructura segmentada se considerara lo siguiente:
 * - Las dependencias seran binarias
 * - No habra forwarding
 * - Si una fase se divide en 3 ciclos y una instrucción ejecuta el ultimo ciclo,
 *   el primer y el segundo ciclo de esa fase puede ser usado por otra instrucción.
 * - Los branches seran resueltos en la fase de decodificación pero para esta resolución
 *   la siguiente instrucción no necesitara que acaba la decodificación del branch sino
 *   que supondremos de un hardware intermedio que nos facilita el resultado del branch
 *   sin tener que hacer stalls para que acabe las de decodificación del branch.
 * - Los stalls en la escalabilidad no tendran discriminación por unidad, sino estos
 *   afectaran y se propagaran hacia abajo independientemente de la unidad funcional.
 * 
 * @author Belen Bermejo Gonzalez
 * @author Enrique Piña 
 * @author Lluc Martorell
 */
public class Pipeline extends Thread implements MIPS {

    public enum pipelineStages {
        FETCH, DECODE, EXECUTION, MEMORY, WRITE, COMMIT
    }

    /*Estructuras principales de la maquina*/
    private static ArrayList<Instruction> listOfIntructions = new ArrayList<>();    // Lista con todas las instrucciones
    private static int PC = 0;                                                      // Program Counter
    private final int SCALAB = ConfigFile.getScalability();                         // Scalabilidad de la maquina
    private Instruction iAct;                                                       // Instrucción del momencto actual
    private boolean stopExecution;                                                  // Boleana de finalización de la ejecución
    
    /*Estructuras auxilires de la maquina*/
    private static final HashMap<Instruction, Integer> carryOn = new HashMap<>();           // Instrucciones que crean dependencias y donde empiezas su wb
    private static final HashMap<Integer, ReducedInstruction> timeline = new HashMap<>();   // Lista de instrucciones segun ejecución, reducidas
    //private static final HashMap<String, Integer> labelAdresses = new HashMap<>();          // Hash con las 
    private static final ArrayList<Integer> instructionFu = new ArrayList<>();              // Lista con la UF con la que es ejecutada cada instrucción
    private boolean dependence;                                                             // Boleana que marca si una instrucción se le genera stalls
    private int myScalab;                                                                   // Marca en que unidad funcional me encuentro en un momento dado
    private int realizedBranches = 0;                                                       // Saltos realizados
    private int tryBranches = 0;                                                            // Saltos ejecutados, realizados o no
    private final int[] CC = new int[6];                                                    // Estructura para controlar los ciclos de cada fase

    /*Stage latency*/
    private final int FETCH = ConfigFile.getFetchCycles();
    private final int DECODE = ConfigFile.getDecodeCycles();
    private final int EXECUTE = ConfigFile.getExecuteCycles();
    private final int MEMORY = ConfigFile.getMemoryCycles();
    private final int WRITE = ConfigFile.getWriteCycles();

    /**
     * Inicialización del Pipeline.
     * 
     * @throws java.io.FileNotFoundException
     */
    public void initPipe() throws FileNotFoundException, IOException {
        listOfIntructions = Architecture.getListOfIntructions();
        myScalab = SCALAB;
        stopExecution = false;
        dependence = false;
        //fillLabelAdresses(); 
        
        for (int i = 0; i < SCALAB; i++) {
            CC[i] = 0;
        }
        
        for (Instruction i : listOfIntructions) {
            carryOn.put(i, -1);
        }
    }

    /**
     * Ejecución del Pipeline.
     * 
     * @throws IOException
     * @throws InterruptedException 
     */
    public void executePipe() throws IOException, InterruptedException {
        initPipe();
        while (!stopExecution) {
            fetch();
            decode();
            execute();
            memory();
            write();
            
            /*Si ya no hay mas UFs volvemos a usar la primera para la
            siguiente instrucción*/
            if (myScalab == 0) {
                myScalab = SCALAB;
            }
            
            /*Añandimos instrucción ejecutada al timeline*/
            timeline.put(timeline.size(), new ReducedInstruction(iAct));
            
            //prints();
        }
        
        /*Ejecutamos la gráfica*/
        postOperations();
    }

    /**
     * Fase de fetch. En la fase de fetch aparte de actualizarla nuestra
     * estructura de ciclos para dicha fase, cogemos la nueva instrucción 
     * actualizamos el PC, ponemos los ciclos de la fase de fetch para esta
     * instrucción dada y ponemos con que unidad funcional se va a ejecutar.
     */
    private void fetch() {
        /*Si la escalabilidad actual esta en la primera unidad funcional
        actualizamos el flujo temporal de la fase de fetch*/
        if (myScalab == SCALAB) { 
            CC[0] = CC[1];
            CC[1] = CC[0] + FETCH;
        }
        
        /*Cogemos instruccion y actualizamos el Program Counter*/
        iAct = listOfIntructions.get(PC);
        PC++;
        
        /*Ciclo de inicio y fin de la Instrucción actual en la fase de fetch*/
        iAct.setSfetch(CC[0]);
        iAct.setFetch(CC[0]+FETCH);
    
        /*Unidad funcional con la cual se ejecutara dicha instrucción*/
        instructionFu.add(myScalab);
    }

    /**
     * Fase de decodificación. En la fase de decodificación, actualiazaremos la estructura
     * de ciclos como en todas las demas fases, teniendo en cuenta tambien si la instrucción 
     * actual le afecta alguna dependencia. Despues resolveremos la dependencia que tenga esa
     * instrucción refrescando la estructura de ciclos dependiendo del carryOn, el cual tendra
     * el ciclo de wb que habra puesto la instrucción generadora de esa dependencia. 
     * Tendremos en cuenta que si la fase de wb es mas grande que la de decodificación, moveremos
     * esta fase lo mas a la derecha posible.
     * Despues de resolver las dependencias, si nuestra instrucción es un branch, pasaremos
     * a resolver ese branch. Como hemos dicho anteriormente suponemos un resolución agresiva
     * del branch de tal manera que el siguiente fetch se podre hacer justo debajo de la 
     * decodificación. 
     * Dando de esta manera finalizada la fase de decodificación.
     * 
     * @throws IOException
     * @throws InterruptedException 
     */
    private void decode() throws IOException, InterruptedException {
        /*Si la escalabilidad actual esta en la primera unidad funcional
        actualizamos el flujo temporal de la fase de decoficación.
        O si la instrucción tiene una dependecia y es la afectada tambien 
        actualizaremos su flujo temporal.*/
        if (myScalab == SCALAB || (iAct.isRAW() && carryOn.get(iAct) != -1 && carryOn.get(iAct) > CC[1])) { 
           CC[2] = CC[1] + DECODE;
        }
        
        /*Resolución de dependencias*/
        if (iAct.isRAW() && carryOn.get(iAct) != -1 && carryOn.get(iAct) > CC[1]) {
            /*La instrucción actual tiene una dependencia*/
            dependence = true;
            
            /*Si decode es mas pequeño que writeback, desplazamos decode hacia el final de writeback
            Primero hay que escribir en el registro y luego leer del registro*/
            if (WRITE > DECODE) {
                CC[2] = carryOn.get(iAct) + WRITE;
                iAct.setSdecode(CC[2] - DECODE);
            } else {
                iAct.setSdecode(carryOn.get(iAct));
            }
            carryOn.put(iAct, -1);
            CC[1] = iAct.getSdecode();  
            CC[2] = CC[1] + DECODE;
        } else {
            /*Inicio de fase si no tiene dependencias*/
            iAct.setSdecode(CC[1]);
        }
        
        /*Fin de fase*/
        iAct.setDecode(CC[2]);
        
        /*Resolución de saltos*/
        if (iAct.isIsJump()) {
            tryBranches++;
            /*Cogemos todos los operandos y todos los valores de los registros*/
            String[] listOperands = iAct.getListOperands();
            HashMap<String, Integer> registers = Architecture.getRegisters();
            
            /*Ponemos los registros a y b a 0 y definimos el target*/
            int ra, rb = 0;
            // si targe es string, cercam al hash de etiquetes sino es 
            // una direccio directa.
            int target = Integer.parseInt(listOperands[listOperands.length - 1]);
            
            /*Cogemos el opcode del salto y operamos el salto*/
            switch (iAct.getOpcode()) {
                case "J":
                    realizedBranches++;
                    PC = target;
                    break;

                case "BLEZ":
                    ra = registers.get(listOperands[0]);
                    if (ra <= rb) {
                        realizedBranches++;
                        PC = target;
                    }
                    break;

                case "BGEZ":
                    ra = registers.get(listOperands[0]);
                    if (ra >= rb) {
                        realizedBranches++;
                        PC = target;
                    }
                    break;

                case "BLTZ":
                    ra = registers.get(listOperands[0]);
                    if (ra < rb) {
                        realizedBranches++;
                        PC = target; 
                    }
                    break;

                case "BGTZ":
                    ra = registers.get(listOperands[0]);
                    if (ra > rb) {
                        realizedBranches++;
                        PC = target;
                    }
                    break;

                case "BEQ":
                    ra = registers.get(listOperands[0]);
                    rb = registers.get(listOperands[1]);
                    if (ra == rb) {
                        realizedBranches++;
                        PC = target;
                    }
                    break;

                case "BNE":
                    ra = registers.get(listOperands[0]);
                    rb = registers.get(listOperands[1]);
                    if (ra != rb) {
                        realizedBranches++;
                        PC = target;
                    }
                    break;
            }
        }
    }

    /**
     * Fase de ejecución. Miraremos como en todas las fases si hay o no que actualizar
     * la estructura de ciclos y luego pasaremos directamente a ejecutar la instrucción,
     * luego pondremos los cicos de la fase de ejecución a la instrucción actual.
     */
    private void execute() {
        /*Si la escalabilidad actual esta en la primera unidad funcional
        actualizamos el flujo temporal de la fase de ejecución.
        O si la instrucción tiene una dependecia y es la afectada tambien 
        actualizaremos su flujo temporal.*/
        if (myScalab == SCALAB || dependence) {
            CC[3] = CC[2] + EXECUTE;
        }
        
        /*Resolvemos la ejecución de la instrucción*/
        switch (iAct.getOpcode()) {
            case "ADD":
                Operations.ADD(iAct);
                break;

            case "ADDI":
                Operations.ADDI(iAct);
                break;

            case "AND":
                Operations.AND(iAct);
                break;

            case "ANDI":
                Operations.ANDI(iAct);
                break;

            case "LB":
                Operations.LB(iAct);
                break;

            case "LH":
                Operations.LH(iAct);
                break;

            case "LW":
                Operations.LW(iAct);
                break;

            case "NOR":
                Operations.NOR(iAct);
                break;

            case "OR":
                Operations.OR(iAct);
                break;

            case "ORI":
                Operations.ORI(iAct);
                break;

            case "SB":
                Operations.SB(iAct);
                break;

            case "SH":
                Operations.SH(iAct);
                break;

            case "SLLV":
                Operations.SLLV(iAct);
                break;

            case "SRLV":
                Operations.SRLV(iAct);
                break;

            case "SW":
                Operations.SW(iAct);
                break;

            case "SUB":
                Operations.SUB(iAct);
                break;

            case "XOR":
                Operations.XOR(iAct);
                break;

            case "XORI":
                Operations.XORI(iAct);
                break;
        }
        
        /*Ciclo de inicio y fin de la Instrucción actual en la fase de ejecución*/
        iAct.setSexec(CC[2]);
        iAct.setExec(CC[3]);
    }

    /**
     * Fase de memoria. Simplemente actualizamos la estructura de ciclos y 
     * le ponemos los ciclos de la fase de memoria a la instrucción actual.
     */
    private void memory() {
        /*Si la escalabilidad actual esta en la primera unidad funcional
        actualizamos el flujo temporal de la fase de memoria.
        O si la instrucción tiene una dependecia y es la afectada tambien 
        actualizaremos su flujo temporal.*/
        if (myScalab == SCALAB || dependence) {
            CC[4] = CC[3] + MEMORY;
        }
        
        /*Ciclo de inicio y fin de la Instrucción actual en la fase de memoria*/
        iAct.setSmem(CC[3]);
        iAct.setMem(CC[4]);
    }

    /**
     * Fase de escritura en registro.
     */
    private void write() {
       /*Si la escalabilidad actual esta en la primera unidad funcional
        actualizamos el flujo temporal de la fase de escritura de registro.
        O si la instrucción tiene una dependecia y es la afectada tambien 
        actualizaremos su flujo temporal.*/ 
        if (myScalab == SCALAB || dependence) {
            CC[5] = CC[4] + WRITE;
        }
        
        /*Ciclo de inicio y fin de la Instrucción actual en la fase 
        de escritura de registro*/
        iAct.setSwr(CC[4]);
        iAct.setWr(CC[5]);
        
        /*La instrucción llega a su ultima fase y desmarcamos la boleana de dependencia*/
        dependence = false;
        
        /*Si esta instrucción genera una dependencia, la pondrmeos en el carryOn
        con su ciclo inicial de escritura de registro, de tal manera que la
        instrucción dependiente puede poner su etapa de decodificación de acuerdo
        con el inicio de writeback de la instrucción generadora de la dependencia*/
        if (iAct.getSignal().size() > 0) {
            Instruction iBlock = listOfIntructions.get(iAct.getSignal().get(0) - 1);
            carryOn.put(iBlock, iAct.getSwr());
        }
        
        /*Cambiamos de unidad funcional para la siguiente instrucción*/
        myScalab--;
        
        /*Si esta es la ultima instrucción marcaremos la boleana
        de final de ejecución de la máquina*/
        if (PC == listOfIntructions.size()) {
            stopExecution = true;
        }
    }
    
//    private void fillLabelAdresses() throws FileNotFoundException, IOException {
//        BufferedReader bf = new BufferedReader(new FileReader(Architecture.srcPath));
//        boolean in = false;
//        int nlinea = 0;
//
//        String line = bf.readLine();
//        while (line != null) {
//            if (in) {
//                if (!line.startsWith("/")) {
//                    String[] aux = line.split(":");
//                    if (Architecture.getBranchPoints().get(aux[0]) != null) {
//                      
//                    }
//                    nlinea++;
//                }
//            }
//            if (line.equals("\t.text") || line.equals(".text")) {
//                in = true;
//            }
//            line = bf.readLine();
//        }
//    }
    
    /**
     * Metodo para mostrar los resultados. Creamos el panel con toda la información necesaria
     * y ajustamos la ventana a las necesidades que nosotros queremos.
     */
    private void postOperations() {
        TimeLine t = new TimeLine(timeline, instructionFu, (float)realizedBranches/(float)tryBranches);

        JFrame j = new JFrame(ConfigFile.getSourcePath()+" Timeline Execution | Scalability: "+SCALAB);

        j.setDefaultCloseOperation(EXIT_ON_CLOSE);

        // Fullscreen
        Toolkit tk = Toolkit.getDefaultToolkit();  
        int xSize = ((int) tk.getScreenSize().getWidth());  
        int ySize = ((int) tk.getScreenSize().getHeight()); 
        j.setSize(xSize,ySize-26);

        j.setResizable(false);

        JScrollPane jp = new JScrollPane(t);
        jp.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        jp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        
        j.getContentPane().add(jp);
        
        j.setVisible(true);
    }
    
    /**
     * Metodo de soporte. Imprime por pantalla información relevante sobre
     * las instrucciones ejecutadas, como su curso temporal y el valor de los
     * registros despues de cada instruccion ejecutada
     */
    private void prints() {
        System.out.println("***********************************");
        System.out.println(iAct.toString());
        System.out.println("Fetch: "+iAct.getSfetch()+" - "+iAct.getFetch());
        System.out.println("Decode: "+iAct.getSdecode()+" - "+iAct.getDecode());
        System.out.println("Exec: "+iAct.getSexec()+" - "+iAct.getExec());
        System.out.println("Memory: "+iAct.getSmem()+" - "+iAct.getMem());
        System.out.println("Wb: "+iAct.getSwr()+" - "+iAct.getWr());
        System.out.println("");
        System.out.println("PC: "+PC);
        System.out.println("");
        System.out.println(Architecture.getRegisters());
        System.out.println("***********************************");
        System.out.println("");
    }
}