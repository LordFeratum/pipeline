package main;

import architecture.Instruction;

/**
 * La clase ReducedInstruction, es una clase creada con el proposito de poder
 * copiar una instrucción, cogiendo de ella lo mas elemental: El nombre de
 * la instrucción en si y sus flujo temporal. Sirve para clonar en un modo reducido
 * las instrucciones, ya que las clase Instructions no dispone de la interficie
 * clonable.
 * Esta clase esta creada solo para poder tener una instrucción ejecutada dos veces
 * con dos valores temporales distintos pero que es la misma instrucción.
 * 
 * @author Enrique Piña 
 * @author Lluc Martorell
 */
public class ReducedInstruction {

    /*Variables */
    private final String name;
    private final int sfetch, fetch;
    private final int sdecode, decode;
    private final int sexec, exec;
    private final int smem, mem;
    private final int swr, wr;
    
    /**
     * Metodo constructor, copiamos lo necesario de la instrucción
     * para luego poder usarla para el TimeLine.
     * 
     * @param original      Instruccion la cual queremos clonar
     */
    public ReducedInstruction(Instruction original) {
        this.name = original.toString();
        this.sfetch = original.getSfetch();
        this.fetch = original.getFetch();
        this.sdecode = original.getSdecode();
        this.decode = original.getDecode();
        this.sexec = original.getSexec();
        this.exec = original.getExec();
        this.smem = original.getSmem();
        this.mem = original.getMem();
        this.swr = original.getSwr();
        this.wr = original.getWr();
    }

    @Override
    public String toString() {
        return name;
    }

    public int getSfetch() {
        return sfetch;
    }

    public int getFetch() {
        return fetch;
    }

    public int getSdecode() {
        return sdecode;
    }

    public int getDecode() {
        return decode;
    }

    public int getSexec() {
        return sexec;
    }

    public int getExec() {
        return exec;
    }

    public int getSmem() {
        return smem;
    }

    public int getMem() {
        return mem;
    }

    public int getSwr() {
        return swr;
    }

    public int getWr() {
        return wr;
    }
}